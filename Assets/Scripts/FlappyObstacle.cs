﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlappyObstacle : MonoBehaviour
{
    private float Limit;
    public float Speed = 5f;

	// Use this for initialization
	void Start ()
	{
        Camera cam = Camera.main;
	    var horzExtent = cam.orthographicSize;
	    Limit = cam.transform.position.x - horzExtent * 2.5f;
	}
	
	// Update is called once per frame
	void Update ()
	{
		transform.Translate(new Vector3(-Speed * Time.deltaTime, 0));
        if(transform.position.x < Limit)
            Destroy(gameObject);
	}
}
