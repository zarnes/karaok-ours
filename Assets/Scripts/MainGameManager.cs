﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainGameManager : MonoBehaviour
{
    public static MainGameManager Instance;

    public int Score = 0;
	// Use this for initialization
	void Start ()
	{
	    // Singleton creation
	    if (Instance == this)
	        return;

	    if (Instance != null)
	    {
	        Destroy(this);
	        return;
	    }

	    DontDestroyOnLoad(gameObject);
	    Instance = this;
    }

    public void ChangeScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);

        switch (sceneName)
        {
            case "Menu":
                Score = 0;
                break;
        }
    }

    public void AddScore(int number)
    {
        Score += number;
    }
}
