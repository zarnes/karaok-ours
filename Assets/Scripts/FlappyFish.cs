﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlappyFish : MonoBehaviour
{
    private MicrophoneListener _listener;
    public float Speed;

	// Use this for initialization
	void Start ()
	{
	    Camera cam = Camera.main;
	    var horzExtent = cam.orthographicSize;

        Vector2 pos = new Vector2(
	        cam.transform.position.x - horzExtent * 1.5f,
	        transform.position.y);
	    transform.position = pos;

		_listener = MicrophoneListener.Instance;
	}
	
	// Update is called once per frame
	void Update ()
	{
		transform.Translate(new Vector3(0, _listener.Input * Speed * Time.deltaTime));
	}

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.name == "Death")
        {
            MainGameManager.Instance.ChangeScene("Snake");
        }
        else if (other.name == "Gate")
        {
            ++MainGameManager.Instance.Score;
        }
    }
}
