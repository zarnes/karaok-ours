﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{
	// Use this for initialization
	void Start ()
	{
		transform.Find("Slider").GetComponent<Slider>().onValueChanged.AddListener((v) => MicrophoneListener.Instance.SetThreshold(v));
	    transform.Find("Slider").GetComponent<Slider>().value = MicrophoneListener.Instance.ControlThreshold;
		transform.Find("Button").GetComponent<Button>().onClick.AddListener(() => MainGameManager.Instance.ChangeScene("Flappy"));
	}
}
