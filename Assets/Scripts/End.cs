﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class End : MonoBehaviour
{
	// Use this for initialization
	void Start ()
	{
	    Transform cv = GameObject.Find("Canvas").transform;
	    cv.Find("Text").GetComponent<Text>().text = MainGameManager.Instance.Score + " points !";
	    cv.Find("Button").GetComponent<Button>().onClick.AddListener(() => MainGameManager.Instance.ChangeScene("Menu"));
    }
}
