﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MicrophoneListener : MonoBehaviour
{
    public static MicrophoneListener Instance;
    
    public int FrequencyPrecision = 1024;
    public float RayLength;
    public float InputSmoothSpeed = 0.5f;
    
    [Range(0.01f, 0.1f)] public float ControlMin;
    [Range(0.01f, 0.03f)] public float ControlThreshold = 0.018f;
    [Range(0.01f, 0.1f)] public float ControlMax;

    [Space]
    [Header("Debug")]
    [Range(-1, 1)]public float DebugInput;

    [Space]
    [Header("Read Only")]
    public float RealInput;
    public float Input;
    private float _currentSmoothVelocity;

    // Value of frequencies
    public float LowerSource;
    public float HigherSource;

    // Device selected to hear voice
    public string SelectedDevice;

    private AudioSource _source;
    private bool _error = false;

	// Use this for initialization
	void Awake()
	{
        // Singleton creation
	    if (Instance == this)
	        return;

	    if (Instance != null)
	    {
            Destroy(this);
	        return;
	    }

        DontDestroyOnLoad(gameObject);
	    Instance = this;

        // Microphone finder
	    int selectedFreq = 0;
        Debug.Log(Microphone.devices.Length + " devices found");
	    foreach (string device in Microphone.devices)
	    {
	        int minFreq;
	        int maxFreq;
            Microphone.GetDeviceCaps(device, out minFreq, out maxFreq);
	        if (maxFreq > 0 && SelectedDevice == "")
	        {
	            Debug.Log("Using " + device + ", " + minFreq + " => " + maxFreq);
                SelectedDevice = device;
	            selectedFreq = maxFreq;
	            break;
	        }
	    }

	    if (SelectedDevice == "")
	    {
	        Debug.LogError("Can't find any microphone !");
	        enabled = false;
            return;
	    }

        // Play microphone sound
	    _source = GetComponent<AudioSource>();
	    _source.clip = Microphone.Start(SelectedDevice, true, 1, selectedFreq);
	    _source.loop = true;
        _source.Play();
	}
	
	// Update is called once per frame
    void Update()
    {
        if (Math.Abs(Math.Abs(DebugInput)) > 0.01f)
        {
            RealInput = Input = DebugInput;
            return;
        }

        if (!CheckLimits())
            return;

        Listen();
        CalculateInput();
        DrawSound();

        Input = Mathf.SmoothDamp(Input, RealInput, ref _currentSmoothVelocity, InputSmoothSpeed);
    }

    /// <summary>
    /// Check if limits are correctly set
    /// </summary>
    /// <returns></returns>
    private bool CheckLimits()
    {
        if (_error)
            return false;

        if (ControlMin > ControlThreshold)
        {
            Debug.LogError("Control Min is superior than Control Threshold");
            return false;
        }

        if (ControlThreshold > ControlMax)
        {
            Debug.LogError("Control Max is inferior than Control Threshold");
            return false;
        }

        return true;
    }

    /// <summary>
    /// Listen microphone and set high and low frequencies values
    /// </summary>
    private void Listen()
    {
        int minLimit = (int)(FrequencyPrecision * ControlMin);
        int limit = (int)(FrequencyPrecision * ControlThreshold);
        int maxLimit = (int) (FrequencyPrecision * ControlMax);
        
        float[] spectrum = new float[FrequencyPrecision];
        AudioListener.GetSpectrumData(spectrum, 0, FFTWindow.Rectangular);
   
        LowerSource = 0;
        HigherSource = 0;
        for (int i = 0; i < spectrum.Length; ++i)
        {
            if (i < minLimit)
                continue;

            if (i > maxLimit)
                break;

            if (i <= limit)
                LowerSource += spectrum[i];
            else
                HigherSource += spectrum[i];
        } 
    }

    /// <summary>
    /// Calculate input with voice frequencies
    /// </summary>
    private void CalculateInput()
    {
        // If only low frequencies, return 1
        // If only high frequencies, return -1

        float total = LowerSource + HigherSource;
        RealInput = (HigherSource / total) * 2 - 1;
    }

    /// <summary>
    /// Debug function to draw voice input
    /// </summary>
    private void DrawSound()
    {
        Debug.DrawLine(new Vector3(0, 0), new Vector3(0, LowerSource * RayLength), Color.red);
        Debug.DrawLine(new Vector3(2, 0), new Vector3(2, HigherSource * RayLength), Color.blue);

        Debug.DrawLine(new Vector3(4, 0), new Vector3(4, RealInput * RayLength / 100), Color.green);
    }

    public void SetThreshold(float value)
    {
        ControlThreshold = value;
    }
}
