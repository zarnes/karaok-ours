﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputCanvas : MonoBehaviour
{
    private MicrophoneListener _listener;

    private RectTransform _negativeBarHorizontal;
    private RectTransform _positiveBarHorizontal;

    private RectTransform _negativeBarVertical;
    private RectTransform _positiveBarVertical;

    private GameObject _horizontal;
    private GameObject _vertical;

    //[Range(-1, 1)] public float Value = 0;

    public bool Horizontal = true;
    public bool Vertical = true;

	// Use this for initialization
	void Awake()
	{
		_listener = MicrophoneListener.Instance;

	    _horizontal = transform.Find("Horizontal Input Panel").gameObject;
	    _vertical = transform.Find("Vertical Input Panel").gameObject;

        _negativeBarHorizontal = transform.Find("Horizontal Input Panel/Negative Bar").GetComponent<RectTransform>();
	    _positiveBarHorizontal = transform.Find("Horizontal Input Panel/Positive Bar").GetComponent<RectTransform>();

	    _negativeBarVertical = transform.Find("Vertical Input Panel/Negative Bar").GetComponent<RectTransform>();
	    _positiveBarVertical = transform.Find("Vertical Input Panel/Positive Bar").GetComponent<RectTransform>();
    }
	
	// Update is called once per frame
	void Update()
	{
        _horizontal.SetActive(Horizontal);
        _vertical.SetActive(Vertical);

	    float fill = _listener.Input / 2 + 0.5f;

	    _negativeBarHorizontal.anchorMin = new Vector2(fill, 0f);
	    _positiveBarHorizontal.anchorMax = new Vector2(fill, 1f);

	    _negativeBarVertical.anchorMin = new Vector2(0f, fill);
	    _positiveBarVertical.anchorMax = new Vector2(1, fill);
    }
}
