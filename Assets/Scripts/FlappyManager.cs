﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlappyManager : MonoBehaviour
{
    public float ObstaclesSpeed = 5f;
    public Transform SpawnPoint;
    private float SpawnX;
    public float DelayBetweenSpawns = 2;

    public GameObject[] Obstacles;

	// Use this for initialization
	void Start ()
	{
	    Camera cam = Camera.main;
	    var horzExtent = cam.orthographicSize;
	    SpawnX = cam.transform.position.x + horzExtent * 2.5f;

        StartCoroutine(SpawnObstacle());

	    GameObject.Find("Input Canvas").GetComponent<InputCanvas>().Horizontal = false;
	    GameObject.Find("Input Canvas").GetComponent<InputCanvas>().Vertical = true;
	}

    public IEnumerator SpawnObstacle()
    {
        while (true)
        {
            yield return new WaitForSeconds(DelayBetweenSpawns);

            GameObject prefab = Obstacles[Random.Range(0, Obstacles.Length)];
            
            Vector2 SpawnPoint = new Vector2(SpawnX, this.SpawnPoint.position.y);
            GameObject obstacle = Instantiate(prefab, SpawnPoint, Quaternion.identity);
            obstacle.GetComponent<FlappyObstacle>().Speed = ObstaclesSpeed;
        }
    }
}
